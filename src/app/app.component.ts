import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  field: string[][];
  counter: number;

  constructor() {
    this.newGame();
  }

  get nextSymbol(): string {
    return this.counter % 2 === 0 ? "X" : "O";
  }

  private newGame() {
    this.field = [];
    for (let i = 0; i <= 2; i++) {
      this.field[i] = [];
    }
    this.counter = 0;
  }

  makeSymbol(row: number, col: number): void {
    if (this.field[row][col] === undefined) { // Wenn Feld ist leer
      this.field[row][col] = this.nextSymbol; // erstelle neues Symbol
      if (this.isWon()) { // Wenn gewonnen
        this.showMessageAndStartNewGame(this.nextSymbol + " hat gewonnen"); // zeige wer gewonnen hat
      } else if (this.counter === 8) {// wenn alle felder voll
        this.showMessageAndStartNewGame("Unentschieden"); // zeige unentschieden
      }
      this.counter++; // ein Feld mehr ist nun voll
    }
  }

  private showMessageAndStartNewGame(message: string) {
    setTimeout(() => {
      alert(message);
      this.newGame();
    }, 100);
  }

  private isWon(): boolean {
    return this.isWonVertically() || this.isWonHorizontally()
      || this.isWonDiagonally();
  }

  private isWonVertically(): boolean {
    for (let i = 0; i <= 2; i++) {
      if (this.areSameAndDefined(this.field[i][0], this.field[i][1], this.field[i][2])) {
        return true;
      }
    }
    return false;
  }

  private isWonHorizontally(): boolean {
    for (let i = 0; i <= 2; i++) {
      if (this.areSameAndDefined(this.field[0][i], this.field[1][i], this.field[2][i])) {
        return true;
      }
    }
    return false;
  }

  private isWonDiagonally(): boolean {
    if (this.areSameAndDefined(this.field[0][0], this.field[1][1], this.field[2][2])
      || this.areSameAndDefined(this.field[2][0], this.field[1][1], this.field[0][2])) {
      return true;
    }
    return false;
  }

  private areSameAndDefined(feld1: string, feld2: string, feld3: string): boolean {
    return feld1 !== undefined && feld1 === feld2 && feld1 === feld3;

  }
}

function prependZeroIfNeccessary(i: number): string {
  if (i < 10) {
    return "0" + i;
  }

  return "" + i;
}
